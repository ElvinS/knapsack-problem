## My solution
Assigned package challenge is a classic example of the [0-1 Knapsack Problem](https://en.wikipedia.org/wiki/Knapsack_problem), which is a NP-Hard problem (the optimization part).

There are many possible solutions for solving Knapsack problem. As a default solution I used a [Dynamic Programming](https://en.wikipedia.org/wiki/Dynamic_programming) approach for solving this problem. This way we can avoid recursion (and memory issues when the problem is big enough), and will solve the problem in pseudo-polynomial time.

## Getting Started

Solution is not hardcoded and I tried to follow ```Program to Interface``` and ```SOLID``` principles. 

Solution separated into small parts:
```
- Data Reading
- Data Parsing
- Data Validation 
- Problem Solving
- Showing Result
```

In order to change ```Packer``` class response at runtime and archive scalability I used ```Strategy``` design pattern. I have created several contracts for data reading, validating and problem solving process. So we can easily implement defined interfaces and change our solution response very easily. 

Below listed some examples:

Defined ```DataReader``` interface in order to change or add new data input source. As a default I implemented a class named ```FileReader``` that reads data from file. Future we can create ```DbReader``` class that data can be read data from database and etc..

Defined ```ModelValidator``` interface in order to add new constraints or validation steps very easily. As a default I implemented a class named ```MobiquityValidator``` that defines constraints required in the document.

Defined ```KnapsackSolver``` interface in order to add other custom algorithms. As a default I implemented a class named ```DPSolver``` that keeps our default ```Dynamic Programming``` solution. Future we can create ```GreedySolver``` or ```Brand&BoundSolver``` class in order to solve our knapsack problem in another way.

So above structure and examples shows us we can add new feature to our solution or change existing solution very easily. Because all parts of solution lives independently.

Our main class ```Packer``` has 2 methods: ```pack``` that required in the challenge and ```packWithCustomTools``` defined by me. Method ```pack``` works as required, it accepts ```absolute file path``` and continues to its work. But method ```packWithCustomTools``` can be used with custom implemented data reader, problem solver and validation paramaters. So method ```pack``` inside calls ```packWithCustomTools``` with default implemented ```FileReader```, ```DPSolver``` and ```MobiquityValidator``` parameters.

### Let's start

In order to use/test system you should use below git command to clone project to your computer:
```
git clone https://ElvinS@bitbucket.org/ElvinS/knapsack-problem.git
```
Otherwise you can download the whole project folder as .zip file from bitbucket page.

### Prerequisites
```
Java 8
Apache Maven 3+
```
### Installing
```
mvn exec:java -Dexec.args="<file_path>"
```
### Running the tests
Solution fully unit tested. In order to run the tests following command should be used:
```
mvn test
```