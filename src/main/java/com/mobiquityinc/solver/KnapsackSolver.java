package com.mobiquityinc.solver;

import com.mobiquityinc.model.Item;
import com.mobiquityinc.model.KnapsackRequest;

import java.util.List;

/**
 * The contract defines default problem solving api.
 *
 * Based on this contract we can create our custom solution classes without rewriting existing ones
 */
public interface KnapsackSolver {
    List<Item> solve(KnapsackRequest request);
}
