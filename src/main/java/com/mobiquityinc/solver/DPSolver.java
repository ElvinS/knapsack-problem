package com.mobiquityinc.solver;

import com.mobiquityinc.model.KnapsackRequest;
import com.mobiquityinc.model.Item;
import com.mobiquityinc.model.Package;

import java.util.*;

/**
 * The class is responsible for solving the knapsack problem using Dynamic Programming
 */
public class DPSolver implements KnapsackSolver {
    /**
     * This method is used to perform the Dynamic Programming algorithm on itself,
     * then uses the result for determining which items were selected and returns selected items.
     *
     * @param request instance of KnapsackRequest
     * @return
     */
    @Override
    public List<Item> solve(KnapsackRequest request) {
        final Package _package = request.getPackage();
        final Item[] items = request.getAvailableItems();

        // sorting available package items by cost ratio descending order
        Arrays.sort(items, (Item item1, Item item2) -> item1.getCostRatio() > item2.getCostRatio() ? -1 : 1);

        int packageWeight = _package.getWeight();

        // defining our "weight" table
        double knapsackTable[][] = new double[items.length + 1][packageWeight + 1];

        int itemIdx = 0;
        int weightIdx = 0;

        // Define array for getting maximum total weight
        while (itemIdx <= items.length) {
            while (weightIdx <= packageWeight) {
                if (itemIdx == 0 || weightIdx == 0) {
                    knapsackTable[itemIdx][weightIdx] = 0;
                } else if (items[itemIdx - 1].getWeight() <= weightIdx) {
                    knapsackTable[itemIdx][weightIdx] =
                            Math.max(knapsackTable[itemIdx - 1][weightIdx], items[itemIdx - 1].getCost() +
                                    knapsackTable[itemIdx - 1][weightIdx - (int) items[itemIdx - 1].getWeight()]);
                } else {
                    knapsackTable[itemIdx][weightIdx] = knapsackTable[itemIdx - 1][weightIdx];
                }

                weightIdx += 1;
            }

            weightIdx = 0;
            itemIdx++;
        }

        // Here we completed filling our "weight" table, then we should iterate over the
        // array in order to find which items were selected for the solution.
        final List<Item> selectedPackageItems = extractSelectedPackageItems(items, packageWeight, knapsackTable);

        return selectedPackageItems;
    }

    /**
     * This method is used for extracting selected items from available items
     *
     * @param items available array of Item
     * @param packageWeight maximum weight of the package
     * @param knapsackTable 2D dimension array, calculated "weight" table
     * @return
     */
    private List<Item> extractSelectedPackageItems(Item[] items, int packageWeight, double[][] knapsackTable) {
        double maxWeight = knapsackTable[items.length][packageWeight];

        int weightIdx;
        int itemIdx;
        final List<Item> selectedPackageItems = new ArrayList<>(items.length / 4);

        weightIdx = packageWeight;
        for (itemIdx = items.length; itemIdx > 0 && knapsackTable[items.length][packageWeight] > 0; itemIdx--) {
            if (maxWeight == knapsackTable[itemIdx - 1][weightIdx]) {
                continue;
            } else {
                selectedPackageItems.add(items[itemIdx - 1]);

                maxWeight = maxWeight - items[itemIdx - 1].getCost();
                weightIdx = weightIdx - (int) items[itemIdx - 1].getWeight();
            }
        }
        return selectedPackageItems;
    }
}