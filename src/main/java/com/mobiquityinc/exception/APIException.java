package com.mobiquityinc.exception;

/*
 * The class represents application default checked exception
 */
public class APIException extends Exception {
    public APIException(String errorMessage) {
        super(errorMessage);
    }
}
