package com.mobiquityinc.reader;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

/**
 * The class is responsible for reading data from file
 */
public class FileReader implements DataReader {

    private final String filePath;

    public FileReader(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public List<String> read() throws IOException {
        return readFile();
    }

    private List<String> readFile() throws IOException {
        final File file = new File(filePath);

        return Files.readAllLines(file.toPath());
    }
}