package com.mobiquityinc.reader;

import java.io.IOException;
import java.util.List;

/**
 * The contract defines default data reading api.
 *
 * Based on this contract we can create our custom data reader classes without rewriting existing ones
 */
public interface DataReader {
    List<String> read() throws IOException;
}
