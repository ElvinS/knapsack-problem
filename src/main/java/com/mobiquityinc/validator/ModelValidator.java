package com.mobiquityinc.validator;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.model.KnapsackRequest;

/**
 * A contract defines default validation api.
 *
 * Based on this contract we can create our custom validator class without rewriting existing ones
 */
public interface ModelValidator {
    void validate(KnapsackRequest request) throws APIException;
}