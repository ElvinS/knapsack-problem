package com.mobiquityinc.validator;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.model.Item;
import com.mobiquityinc.model.KnapsackRequest;
import com.mobiquityinc.model.Package;

import static java.text.MessageFormat.format;

/**
 * The class is responsible for data validation based on given constraints
 */
public class MobiquityValidator implements ModelValidator {
    /**
     * Dedicated constants for given constraints
     */
    private static final int _consPackageMaxWeight = 100;
    private static final int _consPackageItemMaxCount = 15;
    private static final int _consPackageItemMaxWeight = 100;
    private static final int _consPackageItemMaxCost = 100;

    /**
     * This method is used to check given constraints
     *
     * @param request instance of KnapsackRequest
     * @throws APIException when does not match given constraints
     */
    @Override
    public void validate(final KnapsackRequest request) throws APIException {
        validatePackage(request.getPackage());
        validatePackageItems(request.getAvailableItems());
    }

    /**
     * This method is used to check given constraint: Max weight that a package can take is ≤ 100
     *
     * @param _package instance of Package
     * @throws APIException when does not match given constraint
     */
    private void validatePackage(final Package _package) throws APIException {
        if (_package.getWeight() > _consPackageMaxWeight) {
            throw new APIException(format("Package weight should not be over {0}", _consPackageMaxWeight));
        }
    }

    /**
     * This method is used to check given constraint: There might be up to 15 items you need to choose from
     *
     * @param items Array of Items
     * @throws APIException when does not match given constraint
     */
    private void validatePackageItems(final Item[] items) throws APIException {
        if (items.length > _consPackageItemMaxCount) {
            throw new APIException(format("Package\'s number of available items should not be over {0}",
                    _consPackageItemMaxCount));
        }

        for (int i = 0; i < items.length; i++) {
            validatePackageItem(items[i]);
        }
    }

    /**
     * This method is used to check given constraint: Max weight and cost of an item is ≤ 100
     *
     * @param item instance of Item
     * @throws APIException - when does not match given constraint
     */
    private void validatePackageItem(final Item item) throws APIException {
        if (item.getWeight() > _consPackageItemMaxWeight) {
            throw new APIException(format("Package item weight should not be over {0}",
                    _consPackageItemMaxWeight));
        }

        if (item.getCost() > _consPackageItemMaxCost) {
            throw new APIException(format("Package item cost should not be over {0}", _consPackageItemMaxCost));
        }
    }
}
