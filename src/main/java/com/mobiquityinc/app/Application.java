package com.mobiquityinc.app;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.packer.Packer;

import java.io.IOException;

public class Application {
    public static void main(String[] args) {
        try {
            if(args.length == 0) {
                System.out.println("\nPlease type correct file path");
                System.exit(0);
            }

            final String output = Packer.pack(args[0]);
            System.out.println(output);
        } catch (IOException | APIException e) {
            e.printStackTrace();
        }
    }
}