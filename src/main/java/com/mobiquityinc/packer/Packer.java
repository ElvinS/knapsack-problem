package com.mobiquityinc.packer;

import com.mobiquityinc.model.Item;
import com.mobiquityinc.model.KnapsackRequest;
import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.parser.ModelParser;
import com.mobiquityinc.reader.DataReader;
import com.mobiquityinc.reader.FileReader;
import com.mobiquityinc.solver.DPSolver;
import com.mobiquityinc.solver.KnapsackSolver;
import com.mobiquityinc.util.ResultBuilder;
import com.mobiquityinc.validator.MobiquityValidator;
import com.mobiquityinc.validator.ModelValidator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The class is responsible for solving knapsack problem based on any source, algorithm and validation input
 */
public class Packer {

    // For the default problem solver we are choosing Dynamic Programming solution
    private static final KnapsackSolver _defaultSolver = new DPSolver();

    // For the default validator we are choosing validator created based on our requirements
    private static final ModelValidator _defaultValidator = new MobiquityValidator();

    /**
     * This method is used to solve all knapsack problem that keeps in the file with Dynamic Programming approach
     *
     * @param filePath absolute path the test file
     * @return String
     * @throws IOException when occurs problem with given file
     * @throws APIException when occurs parsing or validation problem
     */
    public static String pack(final String filePath) throws IOException, APIException {
        final DataReader reader = new FileReader(filePath);

        return packWithCustomTools(reader, _defaultSolver, _defaultValidator);
    }

    /**
     * This method is used to solve knapsack problem with custom input source, algorithm and validation
     *
     * @param reader instance of DataReader
     * @param solver instance of KnapsackSolver
     * @param validator instance of ModelValidator
     * @return String
     * @throws IOException when occurs problem with given file
     * @throws APIException when occurs parsing or validation problem
     */
    public static String packWithCustomTools(final DataReader reader, final KnapsackSolver solver,
                                             final ModelValidator validator)
            throws IOException, APIException {

        final List<KnapsackRequest> knapsackRequests = readData(reader);

        validateData(knapsackRequests, validator);

        final List<List<Item>> outputList = solve(knapsackRequests, solver);
        final String result = printResult(outputList);

        return result;
    }

    /**
     * This method is used to read all data from any input source
     *
     * @param reader instance of DataReader
     * @return List of KnapsackRequest
     * @throws IOException when occurs problem with given file
     * @throws APIException when occurs parsing problem
     */
    private static List<KnapsackRequest> readData(final DataReader reader) throws IOException, APIException {
        final List<String> dataLines = reader.read();
        final List<KnapsackRequest> knapsackRequests = ModelParser.parseKnapsackRequests(dataLines);

        return knapsackRequests;
    }

    /**
     * This method is used to validate knapsack data with help of given validator
     *
     * @param knapsackRequests list of KnapsackRequest
     * @param validator instance of ModelValidator
     * @throws APIException when occurs validation problem
     */
    private static void validateData(final List<KnapsackRequest> knapsackRequests, final ModelValidator validator)
            throws APIException {
        for (KnapsackRequest request: knapsackRequests) {
            validator.validate(request);
        }
    }

    /**
     * This method is used to solve knapsack problem
     *
     * @param knapsackRequests list of KnapsackRequest
     * @param solver instance of KnapsackSolver
     * @return List of Item list, Item list per knapsack problem
     */
    private static List<List<Item>> solve(final List<KnapsackRequest> knapsackRequests, final KnapsackSolver solver) {
        final List<List<Item>> outputList = new ArrayList<>(knapsackRequests.size());

        for (KnapsackRequest request: knapsackRequests) {
            final List<Item> includedItems = solver.solve(request);
            outputList.add(includedItems);
        }

        return outputList;
    }

    /**
     * This method is used to print result as line by line per knapsack problem
     *
     * @param selectedItems list of List<Item>
     * @return String
     */
    private static String printResult(final List<List<Item>> selectedItems) {
        final StringBuilder resultBuilder = new StringBuilder();

        for (List<Item> items: selectedItems) {
            resultBuilder.append(ResultBuilder.build(items))
            .append("\n");
        }

        return resultBuilder.toString();
    }
}