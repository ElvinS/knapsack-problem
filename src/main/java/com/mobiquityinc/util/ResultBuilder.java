package com.mobiquityinc.util;

import com.mobiquityinc.model.Item;

import java.util.List;
import java.util.stream.Collectors;

/**
 * The class is responsible for building output result
 */
public class ResultBuilder {
    private static final String _emptyResult = "-";
    private static final String _indexDelimiter = ",";

    public static String build(final List<Item> items) {
        if(items.isEmpty())
            return _emptyResult;

        return items.stream()
                .map(item -> String.valueOf(item.getIndex()))
                .collect(Collectors.joining(_indexDelimiter));
    }
}
