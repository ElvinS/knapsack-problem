package com.mobiquityinc.parser;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.model.Item;
import com.mobiquityinc.model.KnapsackRequest;
import com.mobiquityinc.model.Package;

import java.util.ArrayList;
import java.util.List;

/*
 * The class is responsible for parsing list of knapsack requests, related package and package items
 */
public class ModelParser {
    /**
     * Dedicated delimiters for data splitting
     */
    private static final String _packageSplitter = " : ";
    private static final String _itemSplitter = " ";
    private static final String _itemPartSplitter = ",";

    /**
     * This method is used to parse knapsack data line by line
     *
     * @param data list of String
     * @return List of KnapsackRequest
     * @throws APIException when occurs parsing problem
     */
    public static List<KnapsackRequest> parseKnapsackRequests(final List<String> data) throws APIException {
        if (data != null && !data.isEmpty()) {
            final List<KnapsackRequest> knapsackRequests = new ArrayList<>(data.size());
            for (String singleDataLine : data) {
                knapsackRequests.add(parseKnapsackRequest(singleDataLine));
            }

            return knapsackRequests;
        }

        throw new APIException("No data found for parsing");
    }

    /**
     * This method is used to parse KnapsackRequest data
     *
     * @param requestAsString String representation of knapsack request
     * @return KnapsackRequest
     * @throws APIException when occurs parsing problem
     */
    public static KnapsackRequest parseKnapsackRequest(final String requestAsString) throws APIException {
        final String[] contentUnits = requestAsString.split(_packageSplitter);

        final Package _package = parsePackage(contentUnits[0]);
        final Item[] packageItems = parsePackageItems(contentUnits[1]);

        return new KnapsackRequest(_package, packageItems);
    }

    /**
     * This method is used to parse Package data
     *
     * @param packageAsString String representation of package
     * @return Package
     * @throws APIException when occurs parsing problem
     */
    public static Package parsePackage(final String packageAsString) throws APIException {
        try {
            final int weight = Integer.parseInt(packageAsString);

            return new Package(weight);
        } catch (NumberFormatException ex) {
            System.out.println(ex.getMessage());
        }

        throw new APIException("Invalid number for package weight");
    }

    /**
     * This method is used to parse package Items data
     *
     * @param itemsAsString String representation of package items
     * @return Array of Item
     * @throws APIException when occurs parsing problem
     */
    public static Item[] parsePackageItems(final String itemsAsString) throws APIException {
        final String[] itemUnits = itemsAsString.split(_itemSplitter);

        if (itemUnits.length > 0) {
            final Item[] packageItems = new Item[itemUnits.length];
            for (int i = 0; i < itemUnits.length; i++) {
                packageItems[i] = parsePackageItem(itemUnits[i]);
            }

            return packageItems;
        }

        throw new APIException("No data found for parsing package items");
    }

    /**
     * This method is used to parse package Item data
     *
     * @param itemAsString String representation of package item
     * @return Item
     * @throws APIException when occurs parsing problem
     */
    public static Item parsePackageItem(final String itemAsString) throws APIException {
        final String rawContent = itemAsString.substring(1, itemAsString.length() - 1);
        final String[] contentUnits = rawContent.split(_itemPartSplitter);

        try {
            final int index = Integer.parseInt(contentUnits[0]);
            final double weight = Double.parseDouble(contentUnits[1]);

            if (contentUnits[2].length() > 1) {
                final String rawCost = contentUnits[2].substring(1);
                final double cost = Double.parseDouble(rawCost);

                return new Item(index, weight, cost);
            }
        } catch (NumberFormatException | IndexOutOfBoundsException ex) {
            System.out.println(ex.getMessage());
        }

        throw new APIException("Invalid number for package item parameters");
    }
}