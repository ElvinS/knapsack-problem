package com.mobiquityinc.model;

import java.util.Objects;

/**
 * The class represents a package
 */
public class Package {
    private final int weight;

    public Package(int weight) {
        this.weight = weight;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.weight);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        Package that = (Package) obj;

        return Objects.equals(weight, that.weight);
    }

    public int getWeight() {
        return weight;
    }
}