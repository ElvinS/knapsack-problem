package com.mobiquityinc.model;

import java.util.Objects;

/**
 * The class represents a package item
 */
public class Item {
    private final int index;
    private final double weight;
    private final double cost;

    public Item(int index, double weight, double cost) {
        this.index = index;
        this.weight = weight;
        this.cost = cost;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.index, this.weight, this.cost);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        Item that = (Item) obj;

        return Objects.equals(index, that.index)
                && Objects.equals(weight, that.weight)
                && Objects.equals(cost, that.cost);
    }

    public int getIndex() {
        return index;
    }

    public double getWeight() {
        return weight;
    }

    public double getCost() {
        return cost;
    }

    public double getCostRatio() {
        return cost / weight;
    }
}