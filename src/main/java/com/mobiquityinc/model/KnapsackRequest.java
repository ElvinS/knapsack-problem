package com.mobiquityinc.model;

/**
 * The class represents single knapsack request with a package and available package items
 */
public class KnapsackRequest {
    private final Package _package;
    private final Item[] availableItems;

    public KnapsackRequest(Package _package, Item[] availableItems) {
        this._package = _package;
        this.availableItems = availableItems;
    }

    public Package getPackage() {
        return new Package(_package.getWeight());
    }

    public Item[] getAvailableItems() {
        return availableItems;
    }
}