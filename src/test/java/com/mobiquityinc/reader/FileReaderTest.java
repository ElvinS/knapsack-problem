package com.mobiquityinc.reader;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static java.text.MessageFormat.format;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class FileReaderTest {
    private String defautFilePath = format("src{0}test{0}resources{0}package.txt", File.separator);

    private DataReader underTest;

    @Before
    public void setUp() {
        underTest = new FileReader(defautFilePath);
    }

    @Test
    public void test_read() throws IOException {
        // when
        final List<String> lines = underTest.read();

        // then
        assertNotNull(lines);
        assertEquals(lines.size(), 4);
    }
}
