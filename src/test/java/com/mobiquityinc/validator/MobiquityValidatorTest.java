package com.mobiquityinc.validator;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.model.Item;
import com.mobiquityinc.model.KnapsackRequest;
import com.mobiquityinc.model.Package;
import org.junit.Before;
import org.junit.Test;

public class MobiquityValidatorTest {
    private ModelValidator underTest;

    @Before
    public void setUp() {
        underTest = new MobiquityValidator();
    }

    @Test
    public void test_validate() throws APIException {
        // given
        final Package _package = new Package(81);
        final Item[] availableItems = new Item[]{createItem(1, 53.38, 45)};
        final KnapsackRequest request = new KnapsackRequest(_package, availableItems);

        // when
        underTest.validate(request);
    }

    @Test(expected = APIException.class)
    public void test_validateMaxPackageWeight() throws APIException {
        // given
        final Package _package = new Package(101);
        final Item[] availableItems = new Item[]{createItem(1, 53.38, 45)};
        final KnapsackRequest request = new KnapsackRequest(_package, availableItems);

        // when
        underTest.validate(request);
    }

    @Test(expected = APIException.class)
    public void test_validateMaxPackageItemsSize() throws APIException {
        // given
        final Package _package = new Package(81);
        final Item[] availableItems = new Item[20];
        availableItems[0] = createItem(1, 53.38, 45);

        final KnapsackRequest request = new KnapsackRequest(_package, availableItems);

        // when
        underTest.validate(request);
    }

    @Test(expected = APIException.class)
    public void test_validateMaxPackageItemWeight() throws APIException {
        // given
        final Package _package = new Package(81);
        final Item[] availableItems = new Item[]{createItem(1, 101.0, 45)};
        final KnapsackRequest request = new KnapsackRequest(_package, availableItems);

        // when
        underTest.validate(request);
    }

    @Test(expected = APIException.class)
    public void test_validateMaxPackageItemCost() throws APIException {
        // given
        final Package _package = new Package(81);
        final Item[] availableItems = new Item[]{createItem(1, 53.38, 101.0)};
        final KnapsackRequest request = new KnapsackRequest(_package, availableItems);

        // when
        underTest.validate(request);
    }

    private Item createItem(int index, double weight, double cost) {
        return new Item(index, weight, cost);
    }
}