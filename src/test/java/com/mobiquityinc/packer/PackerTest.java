package com.mobiquityinc.packer;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.reader.DataReader;
import com.mobiquityinc.reader.FileReader;
import com.mobiquityinc.solver.DPSolver;
import com.mobiquityinc.solver.KnapsackSolver;
import com.mobiquityinc.validator.MobiquityValidator;
import com.mobiquityinc.validator.ModelValidator;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static java.text.MessageFormat.format;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class PackerTest {
    private String defautFilePath = format("src{0}test{0}resources{0}package.txt", File.separator);

    @Test
    public void test_read() throws IOException, APIException {
        // when
        final String result = Packer.pack(defautFilePath);

        // then
        assertNotNull(result);
        assertEquals(result, "4\n" +
                "-\n" +
                "7,2\n" +
                "8,9\n");
    }

    @Test
    public void test_packWithCustomTools() throws IOException, APIException {
        // given
        final DataReader reader = new FileReader(defautFilePath);
        final KnapsackSolver solver = new DPSolver();
        final ModelValidator validator = new MobiquityValidator();

        // when
        final String result = Packer.packWithCustomTools(reader, solver, validator);

        // then
        assertNotNull(result);
        assertEquals(result, "4\n" +
                "-\n" +
                "7,2\n" +
                "8,9\n");
    }
}
