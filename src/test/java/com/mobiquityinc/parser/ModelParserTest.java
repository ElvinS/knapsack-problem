package com.mobiquityinc.parser;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.model.Item;
import com.mobiquityinc.model.KnapsackRequest;
import com.mobiquityinc.model.Package;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ModelParserTest {
    @Test
    public void test_parseKnapsackRequests() throws APIException {
        // given
        final List<String> data = Arrays.asList("81 : (1,53.38,€45) (2,88.62,€98)", "75 : (1,85.31,€29) (2,14.55,€74)");

        // when
        final List<KnapsackRequest> knapsackRequests = ModelParser.parseKnapsackRequests(data);

        // then
        assertNotNull(knapsackRequests);
        assertTrue(knapsackRequests.size() == 2);
    }

    @Test
    public void test_parseKnapsackRequest() throws APIException {
        // given
        final String input = "81 : (1,53.38,€45) (2,88.62,€98)";

        // when
        final KnapsackRequest request = ModelParser.parseKnapsackRequest(input);

        // then
        assertNotNull(request);
        assertNotNull(request.getPackage());
        assertNotNull(request.getAvailableItems());
        assertTrue(request.getAvailableItems().length == 2);
    }

    @Test
    public void test_parsePackage() throws APIException {
        // given
        final String input = "81";

        // when
        final Package _package = ModelParser.parsePackage(input);

        // then
        assertNotNull(_package);
        assertTrue(_package.getWeight() == 81);
    }

    @Test
    public void test_parsePackageItems() throws APIException {
        // given
        final String input = "(1,53.38,€45) (2,88.62,€98)";

        // when
        final Item[] items = ModelParser.parsePackageItems(input);

        // then
        assertNotNull(items);
        assertTrue(items.length == 2);
        assertTrue(items[0].getIndex() == 1);
        assertTrue(items[0].getWeight() == 53.38);
        assertTrue(items[0].getCost() == 45.0);
        assertTrue(items[0].getCostRatio() == 45.0 / 53.38);
        assertTrue(items[1].getIndex() == 2);
        assertTrue(items[1].getWeight() == 88.62);
        assertTrue(items[1].getCost() == 98);
        assertTrue(items[1].getCostRatio() == 98 / 88.62);
    }

    @Test
    public void test_parsePackageItem() throws APIException {
        // given
        final String input = "(1,53.38,€45)";

        // when
        final Item item = ModelParser.parsePackageItem(input);

        // then
        assertNotNull(item);
        assertTrue(item.getIndex() == 1);
        assertTrue(item.getWeight() == 53.38);
        assertTrue(item.getCost() == 45.0);
        assertTrue(item.getCostRatio() == 45.0 / 53.38);
    }
}