package com.mobiquityinc.solver;

import com.mobiquityinc.model.Item;
import com.mobiquityinc.model.KnapsackRequest;
import com.mobiquityinc.model.Package;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class DPSolverTest {
    private KnapsackSolver underTest;

    @Before
    public void setUp() {
        underTest = new DPSolver();
    }

    @Test
    public void test_solve() {
        // given
        final Package _package = new Package(75);
        final Item[] availableItems = new Item[]{
                createItem(1, 85.31, 29),
                createItem(2, 14.55, 74),
                createItem(3, 3.98, 16),
                createItem(4, 26.24, 55),
                createItem(5, 63.69, 52),
                createItem(6, 76.25, 75),
                createItem(7, 60.02, 74),
                createItem(8, 93.18, 35),
                createItem(9, 89.95, 78)
        };
        final KnapsackRequest request = new KnapsackRequest(_package, availableItems);

        // when
        final List<Item> pickedItems = underTest.solve(request);

        // then
        assertNotNull(pickedItems);
        assertEquals(pickedItems.size(), 2);
        assertEquals(pickedItems.get(0).getIndex(), 7);
        assertEquals(pickedItems.get(1).getIndex(), 2);
    }

    @Test
    public void test_solveForNotSelectedItems() {
        // given
        final Package _package = new Package(8);
        final Item[] availableItems = new Item[]{createItem(1,15.3, 34)};
        final KnapsackRequest request = new KnapsackRequest(_package, availableItems);

        // when
        final List<Item> pickedItems = underTest.solve(request);

        // then
        assertNotNull(pickedItems);
        assertEquals(pickedItems.size(), 0);
    }

    private Item createItem(int index, double weight, double cost) {
        return new Item(index, weight, cost);
    }
}